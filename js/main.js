"use strict"

// Не побачив питання)
// Тому що при використанні голосового вводу, віртуальної клавіатури та ін. 
// неможливо відстежити ці події

window.addEventListener("keypress", checkKey);

function checkKey(event) {
    const key = event.code.split("")[3];
    const btns = document.querySelectorAll(".btn");
    btns.forEach((element)=>{
        if(element.classList.contains('btn-press')){
            element.classList.remove('btn-press');
        }
        if(key===element.textContent || element.textContent === event.key){
            element.classList.add("btn-press");
        }
    });
}